package hr.fer.rznu.homework_2.model

data class Message(
        val message: String
)