package hr.fer.rznu.homework_2.controller

import hr.fer.rznu.homework_2.model.Message
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.stereotype.Controller

@Controller
class ChatController {

    @MessageMapping("/chat.sendMessage")
    @SendTo("/chatroom/public")
    fun sendMessage(@Payload message: Message): Message = message
}