package hr.fer.rznu.homework1.rest.controllers

import hr.fer.rznu.homework1.data.model.User
import hr.fer.rznu.homework1.di.DependencyProvider
import hr.fer.rznu.homework1.rest.SessionAttributes
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@RestController
class SessionController {

    val userRepository = DependencyProvider.provideUserRepository()

    @PostMapping("api/login")
    fun login(req: HttpServletRequest,
              @RequestParam("userName") userName: String = "",
              @RequestParam("password") password: String = ""): ResponseEntity<LoginResponse> {
        val user = userRepository.getUser(userName)
        if(user == User.EMPTY || password.hashCode() != user.passwordHash) {
            println("Invalid Parameters")
            return ResponseEntity(LoginResponse(false), HttpStatus.UNAUTHORIZED)
        } else {
            val session = req.session

            if(session.getAttribute(SessionAttributes.USER_ID) != null) {
                println("Session Already Exists")
                return ResponseEntity(LoginResponse(false), HttpStatus.CONFLICT)
            }
            with(req.session) {
                setAttribute(SessionAttributes.USER_ID, user.userId)
                setAttribute(SessionAttributes.USER_NAME, user.userName)
                setAttribute(SessionAttributes.PASSWORD_HASH, user.passwordHash)
                maxInactiveInterval = 600
                println("Session Created")
            }
            return ResponseEntity(LoginResponse(true), HttpStatus.OK)
        }

    }

    @PostMapping("api/logout")
    fun logout(req: HttpServletRequest): ResponseEntity<LogoutResponse> {
        return if(req.session.getAttribute(SessionAttributes.USER_ID) == null) {
            println("Not Login")
            ResponseEntity(LogoutResponse(false), HttpStatus.BAD_REQUEST)
        } else {
            req.session.invalidate()
            println("Invalidate")
            ResponseEntity(LogoutResponse(true), HttpStatus.OK)
        }
    }

    data class LoginResponse(private val loginStatus: Boolean)
    data class LogoutResponse(private val logoutStatus: Boolean)
}