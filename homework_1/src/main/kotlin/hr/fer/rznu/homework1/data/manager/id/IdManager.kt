package hr.fer.rznu.homework1.data.manager.id

interface IdManager {
    fun getId(): Int
}