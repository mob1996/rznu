package hr.fer.rznu.homework1.data.manager.id

import java.util.concurrent.atomic.AtomicInteger

class IdManagerImpl: IdManager {

    private val availableId = AtomicInteger(0)

    override fun getId(): Int {
        return availableId.getAndIncrement()
    }
}