package hr.fer.rznu.homework1.data.repositories.status

import hr.fer.rznu.homework1.data.manager.id.IdManager
import hr.fer.rznu.homework1.data.model.Status
import java.util.concurrent.ConcurrentHashMap

class StatusRepositoryImpl(private val statusIdManager: IdManager) : StatusRepository {

    private val statuses: MutableMap<Int, Status> = ConcurrentHashMap()

    init {
        createStatus(0, "WoW", "Uldir Raid")
        createStatus(0, "WoW", "Argus Raid")
        createStatus(0, "WoW", "Tomb Raid")
        createStatus(1, "Games", "I love gaming")
        createStatus(2, "TLOU", "The Last of Us")
    }

    override fun createStatus(authorId: Int, statusTitle: String, statusContent: String): Boolean {
        val id = statusIdManager.getId()
        statuses[id] = Status(authorId, id, statusTitle, statusContent)
        return true
    }

    override fun getStatus(statusId: Int): Status =
            statuses.getOrDefault(statusId, Status.EMPTY)


    override fun getStatusesFromUser(userId: Int): List<Status> =
            Sequence { statuses.values.iterator() }
                    .filter { it.authorId == userId }
                    .toList()


    override fun getStatusAuthorId(statusId: Int): Int = statuses.getOrDefault(statusId, Status.EMPTY).authorId

    override fun editStatus(statusId: Int, statusTitle: String, statusContent: String): Boolean {
        var updated = false
        statuses.computeIfPresent(statusId) { _, status ->
            updated = true
            Status(status.authorId, statusId, statusTitle, statusContent)
        }

        return updated
    }

    override fun deleteStatus(statusId: Int): Boolean =
            statuses.remove(statusId) != null

    override fun deleteStatusesFromUser(authorId: Int): Boolean {
        Sequence { statuses.keys.iterator() }
                .forEach { deleteStatus(it) }
        return true
    }

    override fun getAllStatuses(): List<Status> =
            Sequence { statuses.values.iterator() }
                    .toList()

}