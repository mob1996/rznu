package hr.fer.rznu.homework1.rest

import hr.fer.rznu.homework1.data.model.User
import javax.servlet.http.HttpServletRequest

fun HttpServletRequest.isValidSession(user: User): Boolean =
        with(user) {
            if (session.getAttribute(SessionAttributes.USER_ID) == userId
                    && session.getAttribute(SessionAttributes.USER_NAME) == userName
                    && session.getAttribute(SessionAttributes.PASSWORD_HASH) == passwordHash) {
                return true
            }
            return false
        }

fun HttpServletRequest.sessionToMap(): Map<String, Any> {
    val sessionAttributesMap = mutableMapOf<String, Any>()
    session.attributeNames.asIterator()
            .forEach { sessionAttributesMap[it] = session.getAttribute(it)}
    return sessionAttributesMap
}
