package hr.fer.rznu.homework1.rest.controllers

import hr.fer.rznu.homework1.data.model.Index
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse

@RestController
class IndexController {

    @GetMapping("", "index", "index.html")
    fun getIndex(resp: HttpServletResponse) {
        resp.writer.use {
            it.write(Index.index)
            it.flush()
        }
    }
}