package hr.fer.rznu.homework1.data.model

data class Status(val authorId: Int,
                  val statusId: Int,
                  val statusTitle:String,
                  val statusContent: String) {

    companion object {
        val EMPTY = Status(-1, -1, "", "")
    }
}