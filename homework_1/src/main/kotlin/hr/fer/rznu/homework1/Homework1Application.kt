package hr.fer.rznu.homework1

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Homework1Application

fun main(args: Array<String>) {
    runApplication<Homework1Application>(*args)
}
