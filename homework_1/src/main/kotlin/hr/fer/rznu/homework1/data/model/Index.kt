package hr.fer.rznu.homework1.data.model

object Index {

    val index = """<html>
  	<head>
		<style>
			table {
   	 			font-family: arial, sans-serif;
   				border-collapse: collapse;
    			width: 100%;
			}

			td, th {
    			border: 1px solid #dddddd;
    			text-align: left;
    			padding: 8px;
			}

			tr:nth-child(even) {
   				background-color: #dddddd;
			}
		</style>
	</head>

	<body>

      <h1>REST API DOCUMENTATION</h1>

      <h2>SESSION API<h2>
        	<h3> /api/login<h3>
              <table>
                	<tr>
                      <th>Method</th>
                      <th>Parameters</th>
                      <th>Description</th>
                     </tr>
                	<tr>
                      <td>POST</td>
                      	<td>userName, password</td>
                      	<td>Creates active user session</td>
                	</tr>
              </table>
              <h3> /api/logout<h3>
              <table>
                	<tr>
                      <th>Method</th>
                      <th>Parameters</th>
                      <th>Description</th>
                     </tr>
                	<tr>
                      <td>POST</td>
                      	<td>No parameters</td>
                      	<td>Invalidates current user session</td>
                	</tr>
              </table>
    <h2>USER API</h2>
                <h3>/api/user</h3>
                <table>
                	<tr>
                      <th>Method</th>
                      <th>Parameters</th>
                      <th>Description</th>
                     </tr>
                	<tr>
                      	<td>POST</td>
                      	<td>userName, password</td>
                      	<td>Creates new user</td>
                	</tr>
                  	<tr>
                      	<td>GET</td>
                      	<td>No parameters</td>
                      	<td>Retrieve JSON that contains all users</td>
                	</tr>
              </table>

                <h3>/api/user/{userId}</h3>
                <table>
                	<tr>
                      <th>Method</th>
                      <th>Parameters</th>
                      <th>Description</th>
                     </tr>
                	<tr>
                      	<td>GET</td>
                      	<td>No parameters</td>
                      	<td>Retrieve JSON that contains user info</td>
                	</tr>
                  	<tr>
                      	<td>DELETE</td>
                      	<td>No parameters</td>
                      	<td>Deletes user if permission is granted</td>
                	</tr>
                  <tr>
                      	<td>PUT</td>
                      	<td>userName, password</td>
                      	<td>Sets new username and password</td>
                	</tr>
              </table>
                <h3>/api/user/{userId}/status</h3>
                <table>
                	<tr>
                      <th>Method</th>
                      <th>Parameters</th>
                      <th>Description</th>
                     </tr>
                	<tr>
                      	<td>GET</td>
                      	<td>No parameters</td>
                      	<td>Retrieves all statuses from user with {userId}</td>
                	</tr>
              </table>

    	<h2>STATUS API</h2>
                <h3>/api/status</h3>
                <table>
                	<tr>
                      <th>Method</th>
                      <th>Parameters</th>
                      <th>Description</th>
                     </tr>
                	<tr>
                      	<td>POST</td>
                      	<td>statusTitle, statusContent</td>
                      	<td>Creates status and author is decided based on current session</td>
                	</tr>
                  	<tr>
                      	<td>GET</td>
                      	<td>No parameters</td>
                      	<td>Retrieve JSON that contains all statuses</td>
                	</tr>
              </table>

                <h3>/api/status/{statusId}</h3>
                <table>
                	<tr>
                      <th>Method</th>
                      <th>Parameters</th>
                      <th>Description</th>
                     </tr>
                	<tr>
                      	<td>GET</td>
                      	<td>No parameters</td>
                      	<td>Retrieve JSON that contains status info and contents</td>
                	</tr>
                  	<tr>
                      	<td>DELETE</td>
                      	<td>No parameters</td>
                      	<td>Deletes statzs if permission is granted</td>
                	</tr>
                  <tr>
                      	<td>PUT</td>
                      	<td>statusTitle, statusContent</td>
                      	<td>Edits {statusId} status if permission is granted</td>
                	</tr>
              </table>
  	</body>
</html>"""
}