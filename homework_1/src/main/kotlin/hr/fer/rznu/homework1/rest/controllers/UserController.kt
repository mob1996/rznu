package hr.fer.rznu.homework1.rest.controllers

import hr.fer.rznu.homework1.data.model.Status
import hr.fer.rznu.homework1.data.model.User
import hr.fer.rznu.homework1.di.DependencyProvider
import hr.fer.rznu.homework1.rest.SessionAttributes
import hr.fer.rznu.homework1.rest.isValidSession
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
class UserController {

    val userRepository = DependencyProvider.provideUserRepository()
    val statusRepository = DependencyProvider.provideStatusRepository()

    @PostMapping("api/user")
    fun createUser(@RequestParam("userName") userName: String,
                   @RequestParam("password") password: String,
                   resp: HttpServletResponse): ResponseEntity<ReportResponse>? {
        val isCreated = userRepository.createUser(userName, password)
        if (!isCreated) {
            resp.sendError(409, "User with that userName already exists.")
        }

        return ResponseEntity(ReportResponse(true), HttpStatus.CREATED)
    }

    @GetMapping("api/user")
    fun getUsers(): ResponseEntity<List<UserResponse>> =
            ResponseEntity(Sequence { userRepository.getAllUsers().iterator() }
                    .map { UserResponse.fromUser(it) }
                    .toList(), HttpStatus.OK)

    @GetMapping("api/user/{userId}")
    fun getUser(@PathVariable("userId") userId: Int,
                resp: HttpServletResponse): ResponseEntity<UserResponse>? {
        val user = userRepository.getUser(userId)

        return if (user == User.EMPTY) {
            resp.sendError(404, "No Such User")
            null
        } else {
            ResponseEntity(UserResponse.fromUser(user), HttpStatus.OK)
        }
    }

    @GetMapping("api/user/{userId}/status")
    fun getStatusesForUser(@PathVariable("userId") userId: Int,
                           resp: HttpServletResponse): ResponseEntity<List<Status>>? {
        val user = userRepository.getUser(userId)

        return if (user == User.EMPTY) {
            resp.sendError(404, "No Such User")
            null
        } else {
            val statuses = statusRepository.getStatusesFromUser(user.userId)
            ResponseEntity(statuses, HttpStatus.OK)
        }
    }


    @DeleteMapping("api/user/{userId}")
    fun deleteUser(@PathVariable("userId") userId: Int,
                   req: HttpServletRequest,
                   resp: HttpServletResponse): ResponseEntity<ReportResponse>? {
        val user = userRepository.getUser(userId)
        if (user == User.EMPTY) {
            resp.sendError(404, "No such user")
            return null
        }

        if (!req.isValidSession(user)) {
            resp.sendError(403, "You can't delete this user.")
            return null
        }

        val isDeleted = userRepository.deleteUser(userId)
        if (isDeleted) {
            statusRepository.deleteStatusesFromUser(user.userId)
            req.session.invalidate()
        }

        return ResponseEntity(ReportResponse(isDeleted), HttpStatus.OK)
    }

    @PutMapping("api/user/{userId}")
    fun updateUser(@PathVariable("userId") userId: Int,
                   @RequestParam("userName") userName: String,
                   @RequestParam("password") password: String,
                   req: HttpServletRequest,
                   resp: HttpServletResponse): ResponseEntity<ReportResponse>? {
        val user = userRepository.getUser(userId)
        if (user == User.EMPTY) {
            resp.sendError(404, "No such user")
            return null
        }

        if (!req.isValidSession(user)) {
            resp.sendError(403, "You can't update this user.")
            return null
        }

        val isUpdated = userRepository.updateUser(userId, userName, password)
        if (isUpdated) {
            with(req.session) {
                setAttribute(SessionAttributes.USER_NAME, userName)
                setAttribute(SessionAttributes.PASSWORD_HASH, password.hashCode())
            }
        }

        return ResponseEntity(ReportResponse(isUpdated), HttpStatus.OK)
    }

    data class UserResponse(val userId: Int, val userName: String) {
        companion object {
            fun fromUser(user: User) = UserResponse(user.userId, user.userName)
        }
    }

    data class ReportResponse(val success: Boolean)
}