package hr.fer.rznu.homework1.di

import hr.fer.rznu.homework1.data.manager.id.IdManager
import hr.fer.rznu.homework1.data.manager.id.IdManagerImpl
import hr.fer.rznu.homework1.data.repositories.status.StatusRepository
import hr.fer.rznu.homework1.data.repositories.status.StatusRepositoryImpl
import hr.fer.rznu.homework1.data.repositories.user.UserRepository
import hr.fer.rznu.homework1.data.repositories.user.UserRepositoryImpl

object DependencyProvider {

    private lateinit var userIdManager: IdManager
    private lateinit var statusIdManager: IdManager
    private lateinit var userRepository: UserRepository
    private lateinit var statusRepository: StatusRepository

    fun provideUserIdManager(): IdManager {
        if(!::userIdManager.isInitialized) {
            userIdManager = IdManagerImpl()
        }
        return userIdManager
    }

    fun provideStatusIdManager(): IdManager {
        if(!::statusIdManager.isInitialized) {
            statusIdManager = IdManagerImpl()
        }
        return statusIdManager
    }

    fun provideUserRepository(): UserRepository {
        if(!::userRepository.isInitialized) {
            userRepository = UserRepositoryImpl(provideUserIdManager())
        }
        return userRepository
    }

    fun provideStatusRepository(): StatusRepository {
        if(!::statusRepository.isInitialized) {
            statusRepository = StatusRepositoryImpl(provideStatusIdManager())
        }

        return statusRepository
    }
}
