package hr.fer.rznu.homework1.data.repositories.user

import hr.fer.rznu.homework1.data.manager.id.IdManager
import hr.fer.rznu.homework1.data.model.User
import java.util.concurrent.ConcurrentHashMap

class UserRepositoryImpl(private val userIdManager: IdManager): UserRepository {

    private val users: MutableMap<Int, User> = ConcurrentHashMap()

    init {
        createUser("LocDog", "12345")
        createUser("lm48671", "12345")
        createUser("mob-_-", "54321")
        createUser("ferko", "fer12345")
    }

    override fun createUser(userName: String, password: String): Boolean {
        val userNameExists = Sequence { users.iterator() }
                .map { it.value.userName }
                .contains(userName)

        if(userNameExists) {
            return false
        }

        val id = userIdManager.getId()
        users[id] = User(id, userName, password.hashCode())
        return true
    }

    override fun getUser(userId: Int): User =
        users.getOrDefault(userId, User.EMPTY)


    override fun getUser(userName: String): User {
        var user = Sequence { users.iterator() }
                .map { it.value }
                .find { it.userName == userName }

        return if(user == null ) User.EMPTY else user
    }

    override fun deleteUser(userId: Int): Boolean {
        return users.remove(userId) != null
    }

    override fun updateUser(userId:Int, userName: String, password: String): Boolean {
        var updated = false
        users.computeIfPresent(userId) { _, _ ->
            updated = true
            User(userId, userName, password.hashCode())
        }

        return updated
    }

    override fun getAllUsers(): List<User> =
        Sequence { users.iterator() }
                .map { it.value }
                .toList()

}