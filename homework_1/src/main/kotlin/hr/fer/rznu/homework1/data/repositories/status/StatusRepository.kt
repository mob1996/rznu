package hr.fer.rznu.homework1.data.repositories.status

import hr.fer.rznu.homework1.data.model.Status

interface StatusRepository {

    fun createStatus(authorId: Int, statusTitle: String, statusContent: String): Boolean
    fun getStatus(statusId: Int): Status
    fun getStatusesFromUser(userId: Int): List<Status>
    fun getStatusAuthorId(statusId: Int): Int
    fun editStatus(statusId: Int, statusTitle: String, statusContent: String): Boolean
    fun deleteStatus(statusId: Int): Boolean
    fun deleteStatusesFromUser(authorId:Int): Boolean
    fun getAllStatuses(): List<Status>

}