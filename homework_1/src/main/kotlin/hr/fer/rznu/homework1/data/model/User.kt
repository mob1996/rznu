package hr.fer.rznu.homework1.data.model

data class User(val userId:Int,
                val userName:String,
                val passwordHash: Int) {
    companion object {
        val EMPTY = User(-1, "", -1)
    }

}