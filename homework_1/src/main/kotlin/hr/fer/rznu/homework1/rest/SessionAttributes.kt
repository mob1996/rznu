package hr.fer.rznu.homework1.rest

class SessionAttributes {

    companion object {
        val USER_ID = "id"
        val USER_NAME = "user_name"
        val PASSWORD_HASH = "pwd_hash"
    }
}