package hr.fer.rznu.homework1.data.repositories.user

import hr.fer.rznu.homework1.data.model.User

interface UserRepository {

    fun createUser(userName: String, password: String): Boolean
    fun getUser(userId: Int): User
    fun getUser(userName: String): User
    fun deleteUser(userId: Int): Boolean
    fun updateUser(userId:Int, userName: String, password: String): Boolean
    fun getAllUsers(): List<User>
}