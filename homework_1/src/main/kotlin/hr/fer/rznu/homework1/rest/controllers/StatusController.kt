package hr.fer.rznu.homework1.rest.controllers

import hr.fer.rznu.homework1.data.model.Status
import hr.fer.rznu.homework1.di.DependencyProvider
import hr.fer.rznu.homework1.rest.SessionAttributes
import hr.fer.rznu.homework1.rest.isValidSession
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
class StatusController {

    val userRepository = DependencyProvider.provideUserRepository()
    val statusRepository = DependencyProvider.provideStatusRepository()

    @PostMapping("api/status")
    fun createStatus(@RequestParam("statusTitle") statusTitle: String,
                     @RequestParam("statusContent") statusContent: String,
                     req: HttpServletRequest,
                     resp: HttpServletResponse): ResponseEntity<ReportResponse>? {
        val userIdAny = req.session.getAttribute(SessionAttributes.USER_ID)
        if (userIdAny == null) {
            resp.sendError(401, "Unauthorized. Please log in first.")
            return null
        }

        val user = userRepository.getUser(userIdAny as Int)
        if (!req.isValidSession(user)) {
            resp.sendError(409, "Invalid Session")
            return null
        }

        return ResponseEntity(ReportResponse(statusRepository.createStatus(user.userId,
                statusTitle,
                statusContent)), HttpStatus.CREATED)
    }

    @GetMapping("api/status")
    fun getStatuses(): ResponseEntity<List<Status>> {
        return ResponseEntity(statusRepository.getAllStatuses(), HttpStatus.OK)
    }

    @GetMapping("api/status/{statusId}")
    fun getStatus(@PathVariable("statusId") statusId: Int,
                  resp: HttpServletResponse): ResponseEntity<Status>? {
        val status = statusRepository.getStatus(statusId)
        if (status == Status.EMPTY) {
            resp.sendError(404, "Status with that id doesn't exist.")
            return null
        }

        return ResponseEntity(status, HttpStatus.OK)
    }

    @PutMapping("api/status/{statusId}")
    fun editStatus(@PathVariable("statusId") statusId: Int,
                   @RequestParam("statusTitle") statusTitle: String,
                   @RequestParam("statusContent") statusContent: String,
                   req: HttpServletRequest,
                   resp: HttpServletResponse): ResponseEntity<ReportResponse>? {
        val status = statusRepository.getStatus(statusId)

        if (status == Status.EMPTY) {
            resp.sendError(404, "Status with that id doesn't exist.")
            return null
        }

        val author = userRepository.getUser(status.authorId)
        if (!req.isValidSession(author)) {
            resp.sendError(401, "You are not authorized for changing this status.")
            return null
        }

        return ResponseEntity(ReportResponse(statusRepository.editStatus(statusId, statusTitle, statusContent)),
                HttpStatus.OK)
    }

    @DeleteMapping("api/status/{statusId}")
    fun deleteStatus(@PathVariable("statusId") statusId: Int,
                   req: HttpServletRequest,
                   resp: HttpServletResponse): ResponseEntity<ReportResponse>? {
        val status = statusRepository.getStatus(statusId)

        if (status == Status.EMPTY) {
            resp.sendError(404, "Status with that id doesn't exist.")
            return null
        }

        val author = userRepository.getUser(status.authorId)
        if (!req.isValidSession(author)) {
            resp.sendError(401, "You are not authorized for deleting this status.")
            return null
        }

        return ResponseEntity(ReportResponse(statusRepository.deleteStatus(statusId)),
                HttpStatus.OK)
    }

    data class ReportResponse(val success: Boolean)
}