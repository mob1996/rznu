package hr.fer.rznu.homework1

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import hr.fer.rznu.homework1.data.model.Status
import hr.fer.rznu.homework1.di.DependencyProvider
import hr.fer.rznu.homework1.rest.controllers.SessionController
import hr.fer.rznu.homework1.rest.controllers.StatusController
import hr.fer.rznu.homework1.rest.sessionToMap
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc

@RunWith(SpringRunner::class)
@WebMvcTest(controllers = [StatusController::class, SessionController::class])
@AutoConfigureMockMvc(secure = false)
class StatusControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    private lateinit var userSession1: Map<String, Any>
    private lateinit var userSession2: Map<String, Any>

    private val userRepository = DependencyProvider.provideUserRepository()
    private val statusRepository = DependencyProvider.provideStatusRepository()

    private val objectMapper = ObjectMapper().registerModule(KotlinModule())


    @Before
    fun setUp() {
        userSession1 = mockMvc.perform(MMRB.post("/api/login?userName=LocDog&password=12345"))
                .andReturn().request.sessionToMap()
        userSession2 = mockMvc.perform(MMRB.post("/api/login?userName=lm48671&password=12345"))
                .andReturn().request.sessionToMap()
    }

    @After
    fun invalidateSessions() {
        if (!userSession1.isEmpty()) {
            mockMvc.perform(MMRB.post("/api/logout").sessionAttrs(userSession1))
        }
        if (!userSession2.isEmpty()) {
            mockMvc.perform(MMRB.post("/api/logout").sessionAttrs(userSession2))
        }
    }

    @Test
    fun createStatusWithoutSession() {
        mockMvc.perform(MMRB.post("/api/status?statusTitle=TestStatus&statusContent=test"))
                .andExpect(MMRM.status().isUnauthorized)
    }

    @Test
    fun createStatus() {
        val size = statusRepository.getStatusesFromUser(1).size
        mockMvc.perform(MMRB.post("/api/status?statusTitle=TestStatus&statusContent=test")
                .sessionAttrs(userSession2))
                .andExpect(MMRM.status().isCreated)
        assert((size + 1) == statusRepository.getStatusesFromUser(1).size)
    }

    @Test
    fun getStatuses() {
        val body = mockMvc.perform(MMRB.get("/api/status"))
                .andExpect(MMRM.status().isOk).andReturn().response.contentAsString
        val statuses: List<Status> = objectMapper.readValue(body)
        val repoStatuses = statusRepository.getAllStatuses()

        assert(statuses.size == repoStatuses.size)
        for (i in 0..(statuses.size - 1)) {
            assert(statuses[i] == repoStatuses[i])
        }
    }

    @Test
    fun getNonExistentStatus() {
        mockMvc.perform(MMRB.get("/api/status/25"))
                .andExpect(MMRM.status().isNotFound)
    }

    @Test
    fun getStatus() {
        val body = mockMvc.perform(MMRB.get("/api/status/0"))
                .andExpect(MMRM.status().isOk).andReturn().response.contentAsString
        val status: Status = objectMapper.readValue(body)
        val repoStatus = statusRepository.getStatus(0)
        assert(status == repoStatus)
    }

    @Test
    fun editNonExistentStatus() {
        mockMvc.perform(MMRB.put("/api/status/25?statusTitle=WooooW&statusContent=TombOfSargeras"))
                .andExpect(MMRM.status().isNotFound)
    }

    @Test
    fun editStatusWithoutPermission() {
        mockMvc.perform(MMRB.put("/api/status/2?statusTitle=WooooW&statusContent=TombOfSargeras"))
                .andExpect(MMRM.status().isUnauthorized)
    }

    @Test
    fun editStatus() {
        val originalStatus = statusRepository.getStatus(2)
        val wantedStatus = Status(0, 2, "WooooW", "TombOfSargeras")
        mockMvc.perform(MMRB.put("/api/status/2?statusTitle=WooooW&statusContent=TombOfSargeras")
                .sessionAttrs(userSession1))
                .andExpect(MMRM.status().isOk)
        val status = statusRepository.getStatus(2)
        assert(originalStatus != status)
        assert(status == wantedStatus)
    }

    @Test
    fun deleteNonExistentStatus() {
        mockMvc.perform(MMRB.delete("/api/status/25"))
                .andExpect(MMRM.status().isNotFound)
    }

    @Test
    fun deleteStatusWithoutPermission() {
        mockMvc.perform(MMRB.delete("/api/status/3"))
                .andExpect(MMRM.status().isUnauthorized)
    }

    @Test
    fun deleteStatus() {
        mockMvc.perform(MMRB.delete("/api/status/3")
                .sessionAttrs(userSession2))
                .andExpect(MMRM.status().isOk)
        assert(Status.EMPTY == statusRepository.getStatus(3))
    }
}