package hr.fer.rznu.homework1

import hr.fer.rznu.homework1.rest.controllers.SessionController
import hr.fer.rznu.homework1.rest.sessionToMap
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc

@RunWith(SpringRunner::class)
@WebMvcTest(controllers = [SessionController::class])
@AutoConfigureMockMvc(secure = false)
class SessionControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    fun loginWithInvalidCredentials() {
        mockMvc.perform(MMRB.post("/api/login?userName=failTest&password=fail"))
                .andExpect(MMRM.status().isUnauthorized)
    }

    @Test
    fun loginWithValidCredentials() {
        val result = mockMvc.perform(MMRB.post("/api/login?userName=LocDog&password=12345"))
                .andExpect(MMRM.status().isOk)
        mockMvc.perform(MMRB.post("/api/login?userName=lm48671&password=12345")
                .sessionAttrs(result.andReturn().request.sessionToMap()))
                .andExpect(MMRM.status().isConflict)

        mockMvc.perform(MMRB.post("/api/logout")
                .sessionAttrs(result.andReturn().request.sessionToMap()))
                .andExpect(MMRM.status().isOk)
    }

    @Test
    fun logoutWhileNotSignedIn() {
        mockMvc.perform(MMRB.post("/api/logout"))
                .andExpect(MMRM.status().isBadRequest)
    }
}