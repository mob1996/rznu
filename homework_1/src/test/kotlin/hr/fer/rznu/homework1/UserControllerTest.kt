package hr.fer.rznu.homework1

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import hr.fer.rznu.homework1.data.model.Status
import hr.fer.rznu.homework1.di.DependencyProvider
import hr.fer.rznu.homework1.rest.controllers.SessionController
import hr.fer.rznu.homework1.rest.controllers.UserController
import hr.fer.rznu.homework1.rest.sessionToMap
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

typealias MMRB = MockMvcRequestBuilders
typealias MMRM = MockMvcResultMatchers

@RunWith(SpringRunner::class)
@WebMvcTest(controllers = [UserController::class, SessionController::class])
@AutoConfigureMockMvc(secure = false)
class UserControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    private lateinit var userSession1: Map<String, Any>
    private lateinit var userSession2: Map<String, Any>

    private val userRepository = DependencyProvider.provideUserRepository()
    private val statusRepository = DependencyProvider.provideStatusRepository()

    private val objectMapper = ObjectMapper().registerModule(KotlinModule())


    @Before
    fun setUp() {
        userSession1 = mockMvc.perform(MMRB.post("/api/login?userName=mob-_-&password=54321"))
                    .andReturn().request.sessionToMap()
        userSession2 = mockMvc.perform(MMRB.post("/api/login?userName=ferko&password=fer12345"))
                    .andReturn().request.sessionToMap()
    }

    @After
    fun invalidateSessions() {
        if(!userSession1.isEmpty()) {
            mockMvc.perform(MMRB.post("/api/logout").sessionAttrs(userSession1))
        }
        if(!userSession2.isEmpty()) {
            mockMvc.perform(MMRB.post("/api/logout").sessionAttrs(userSession2))
        }
    }

    @Test
    fun createAlreadyExistingUser() {
        val size = userRepository.getAllUsers().size
        mockMvc.perform(MMRB.post("/api/user?userName=LocDog&password=12345"))
               .andExpect(MMRM.status().isConflict)

        assert(size == userRepository.getAllUsers().size)
    }

    @Test
    fun createNewUser() {
        val size = userRepository.getAllUsers().size
        mockMvc.perform(MMRB.post("/api/user?userName=test1&password=test12345"))
                .andExpect(MMRM.status().isCreated)
        assert((size + 1) == userRepository.getAllUsers().size)
    }

    @Test
    fun getUsers() {
        val body = mockMvc.perform(MMRB.get("/api/user"))
                .andExpect(MMRM.status().isOk).andReturn().response.contentAsString
        val users: List<UserController.UserResponse> = objectMapper.readValue(body)
        val repoUsers = userRepository.getAllUsers().map { UserController.UserResponse.fromUser(it) }
        assert(users.size == repoUsers.size)
        for(i in 0..(users.size - 1)) {
            assert(users[i] == repoUsers[i])
        }
    }

    @Test
    fun getNonExistingUser() {
        mockMvc.perform(MMRB.get("/api/user/25"))
                .andExpect(MMRM.status().isNotFound)
    }

    @Test
    fun getExistingUser() {
        val body = mockMvc.perform(MMRB.get("/api/user/0"))
                .andExpect(MMRM.status().isOk).andReturn().response.contentAsString
        val user: UserController.UserResponse = objectMapper.readValue(body)
        val repoUser = UserController.UserResponse.fromUser(userRepository.getUser(user.userId))

        assert(user == repoUser)
    }

    @Test
    fun getStatusesForNonExistingUser() {
        mockMvc.perform(MMRB.get("/api/user/25/status"))
                .andExpect(MMRM.status().isNotFound)
    }

    @Test
    fun getStatusesForUser() {
        val body = mockMvc.perform(MMRB.get("/api/user/0/status"))
                .andExpect(MMRM.status().isOk).andReturn().response.contentAsString
        val statuses: List<Status> = objectMapper.readValue(body)
        val repoStatuses = statusRepository.getStatusesFromUser(0)

        assert(statuses.size == repoStatuses.size)
        for(i in 0..(statuses.size - 1)) {
            assert(statuses[i] == repoStatuses[i])
        }
    }

    @Test
    fun deleteNonExistentUser() {
        mockMvc.perform(MMRB.delete("/api/user/25"))
                .andExpect(MMRM.status().isNotFound)
    }

    @Test
    fun deleteWithoutPermission() {
        mockMvc.perform(MMRB.delete("/api/user/0")
                .sessionAttrs(userSession2))
                .andExpect(MMRM.status().isForbidden)
    }

    @Test
    fun deleteUser() {
        mockMvc.perform(MMRB.delete("/api/user/3")
                .sessionAttrs(userSession2))
                .andExpect(MMRM.status().isOk)

        mockMvc.perform(MMRB.get("/api/user/3"))
                .andExpect(MMRM.status().isNotFound)
    }

    @Test
    fun updateNonExistentUser() {
        mockMvc.perform(MMRB.put("/api/user/25?userName=Mob1996&password=mob123"))
                .andExpect(MMRM.status().isNotFound)
    }

    @Test
    fun updateWithoutPermission() {
        mockMvc.perform(MMRB.put("/api/user/2?userName=Mob1996&password=mob123"))
                .andExpect(MMRM.status().isForbidden)
    }

    @Test
    fun updateUser() {
        mockMvc.perform(MMRB.put("/api/user/2?userName=Mob1996&password=mob123")
                .sessionAttrs(userSession1))
                .andExpect(MMRM.status().isOk)

        val body = mockMvc.perform(MMRB.get("/api/user/2"))
                .andExpect(MMRM.status().isOk).andReturn().response.contentAsString
        val user: UserController.UserResponse = objectMapper.readValue(body)
        assert(user.userName == "Mob1996")
    }
}